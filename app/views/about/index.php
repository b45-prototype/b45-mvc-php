<div class="container">
    <div class="jumbotron">
        <h1 class="display-4">About Me</h1>
        <img src="<?= BASEURL; ?>/img/profile.jpg" alt="Basofi" width="200"  >
        <p>Halo, nama saya
            <?= $data['nama']; ?>, pekerjaan saya adalah seorang
            <?= $data['pekerjaan'];?>.
        </p>
        <hr class="my-4">
        <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
    </div>

</div>