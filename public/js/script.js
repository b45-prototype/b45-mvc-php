$(function () {

    // jquery carikan element yang memmiliki class tombolTambahData ketika di click,
    // jalankan function berikut
    $('.tombolTambahData').on('click', function () {
        $('#formModalLabel').html('Tambah Data Mahasiswa');
        $('.modal-footer button[type=submit]').html('Tambah data');
        $('#nama').val('');
        $('#nim').val('');
        $('#email').val('');
        $('#jurusan').val('');
        $('#id').val('');
    })

    // jquery carikan element yang nama class tampilModalUbah ketika di click,
    // jalankan fungsi ....
    $('.tampilModalUbah').on('click', function () {
        // jquery carikan element yang memiliki id formModalLabel ubah isinya jadi data
        // ubah data mahasiswa
        $('#formModalLabel').html('Ubah Data Mahasiswa');

        // jquery carikan element  yang memiliki class modal-footer dan ambil button
        // yang memiliki tipe submit kemudian ganti isinya dengan ubah data
        $('.modal-footer button[type=submit]').html('Ubah data');

        $('.modal-body form').attr('action', 'http://localhost/phpmvc/public/Mahasiswa/ubah');

        // $(this)merupakan tombol yang di click  / $('.tampilModalUbah').on('click',
        // function ()
        const id = $(this).data('id');
        // console.log(id);

        $.ajax({
            url: 'http://localhost/phpmvc/public/Mahasiswa/getUbah',
            data: {
                id: id
            },
            method: 'post',
            dataType: 'json',
            success: function (data) {
                // console.log(data);
                $('#nama').val(data.nama);
                $('#nim').val(data.nim);
                $('#email').val(data.email);
                $('#jurusan').val(data.jurusan);
                $('#id').val(data.id);
            }
        });

        // $('.tombolTambahData').on('click', function () {
        //     $('#formModalLabel').html('Tambah Data Mahasiswa');
        //     $('.modal-footer button[type=submit]').html('Tambah Data');
        //     $('#nama').val('');
        //     $('#nim').val('');
        //     $('#email').val('');
        //     $('#jurusan').val('');
        //     $('#id').val('');
        // });
    });
});